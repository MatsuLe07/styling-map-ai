from __future__ import print_function

import Keras
from Keras.datasets import mnist
from Keras.models import Sequential
from Keras.layers import Dense, Dropout
from Keras.optimizers import RMSprop


batch_size = 128
num_classes = 10
epochs = 3

# the data, shuffled and split between train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_train = x_train.reshape(60000, 784)
x_test = x_test.reshape(10000, 784)
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
y_train = Keras.utils.to_categorical(y_train, num_classes)
y_test = Keras.utils.to_categorical(y_test, num_classes)


### add for TensorBoard
import Keras.callbacks
import Keras.backend.tensorflow_backend as KTF
import tensorflow as tf

old_session = KTF.get_session()

session = tf.Session('')
KTF.set_session(session)
KTF.set_learning_phase(1)
### 

model = Sequential()
model.add(Dense(512, activation='relu', input_shape=(784,)))
model.add(Dropout(0.2))
model.add(Dense(512, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(10, activation='softmax'))

model.summary()

model.compile(loss='categorical_crossentropy',
              optimizer=RMSprop(),
              metrics=['accuracy'])



### add for TensorBoard
tb_cb = Keras.callbacks.TensorBoard(log_dir="~/tflog/", histogram_freq=1)
cbks = [tb_cb]
###


history = model.fit(x_train, y_train,
                    batch_size=batch_size,
                    epochs=epochs,
                    verbose=1,

                    ## add 1 line
                    callbacks=cbks,

                    validation_data=(x_test, y_test))
score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])


### add for TensorBoard
KTF.set_session(old_session)
###