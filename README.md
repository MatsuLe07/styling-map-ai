# Styling Map AI #

"Styling Map AI" は [qqwweee/keras-yolo3](https://github.com/qqwweee/keras-yolo3)を用いた洋服のテイスト識別 AI である。
***
## ABOUT ##
「カワイイ」「かっこいい」「おしゃれ」といった今までロジカルに説明出来なかった、いわゆる「ファッションセンス」を数値化したものが存在する。
[一般社団法人 日本ファッションスタイリスト協会](https://stylist-kyokai.jp/)が開発、提供している「[Styling Map](https://stylist-kyokai.jp/stylingmap)」である。
「[Styling Map](https://stylist-kyokai.jp/stylingmap)」とは、服それぞれの「色」「形」「素材」等の組み合わせによって「アクアテイスト (Aq)」「クリスタルテイスト (Cr)」「ブライトテイスト (Br)」「アーステイスト (Ea)」の4つに分類するための考え方である。また、テイストが複数存在する服 (「Aq」と「Cr」が混合した服や「Cr」と「Br」と「Ea」が混合した服) なども存在しており、これらを全て含めると 15 種類 (A~O)のテイストに分類することができる。
今回のプログラムでは、洋服のコーディネート画像を教師データとして、AIが自動的にいろんなコーディネートのテイストを判断できるようにした。

## DATA and DEV ##
### DATA ###
>使用した画像 : UNIQLO の Web サイトより、背景が白色のコーディネート画像 2862 枚

>教師データの収集方法 : 日本ファッションスタイリスト協会の代表理事であり、Styling Mapの開発者である、あいざわあゆみ様がすべての画像をラベル分けして提供してくださいました。

| Taste | sheets |
|:------|:-------|
| A     | 169    |
| B     | 141    |
| C     | 194    |
| D     | 550    |
| E     | 205    |
| F     | 274    |
| G     | 113    |
| H     | 385    |
| I     | 230    |
| J     | 160    |
| K     | 174    |
| L     | 119    |
| M     | 67     |
| N     | 40     |
| O     | 41     |
| 計    | 2862   |

### DEV ###
>python : 3.5.6

>cuda : Ver.9.0

>cudnn : Ver.7.0.5

>tensorflow-gpu : 1.6.0

>Keras : 2.1.5

## DEMO ##
入力画像として、以下のコーディネート画像 (GU の Web サイトより) を用いるとする。

![image1](demo_image/demo_image/image1.jpg)

この画像のテイストの分類を行うことで以下のような出力画像が出てくる。

![result1](demo_image/demo_result/result1.jpg)

結果は四角く囲われた画像の左上に表示される。(今回は「I」と識別)

## How to Use ##
### 出力結果をそのまま表示する場合 ###
>まずはじめに、以下のコマンドを入力する。

>`python yolo_video.py --image`

>しばらくすると`Input image filename:`と表示されるため、画像のディレクトリを入力する (画像をコマンドプロンプトにドラッグでも可)。

>その後出力結果が表示される。

### 出力結果を保存する場合 ###
>まずはじめに、以下のコマンドを入力する。

>`python yolo_video.py --input (targetImageDir) --output (outputImageDir)`

>例として、[targetImageDir] = hogehoge/fuga.jpg, [outputImageDir] = hogehoge/piyo.jpg と入力したとする。この場合、hogehoge/fuga.jpg の画像からテイストを識別して hogehoge/piyo.jpg に保存される。
***
## Caution ##
- 開発環境では GPU を用いているため、CPU 環境で実行する場合は yolo.py の 30 行目の`"gpu_num" : 1`を`"gpu_num" : 0`にすれば良い。

- 学習させた画像は全て背景が白色のものを使用したため、現状は白色の背景のみに対応。